defmodule TrixtaAgents.PasswordConstraintTest do
  use ExUnit.Case
  import ExUnit.CaptureLog

  alias TrixtaAgents.Agent

  setup do
    init_state = %TrixtaAgents.Agent{
      id: "password_test_agent",
      name: "Password Test Agent",
      description: "Password Test Agent",
      current_state: :initialising,
      role_ids: [],
      space_ids: []
    }

    agent = Agent.new(init_state)

    [agent: agent]
  end

  test "acceptable passwords get validated correctly" do
    assert Agent.check_password_constraints("hello123") === :ok
    assert Agent.check_password_constraints("p@55w0rd") === :ok
  end

  test "empty passwords get flagged" do
    assert Agent.check_password_constraints("") === {:error, "The password cannot be empty."}
  end

  test "non-string passwords get flagged" do
    assert Agent.check_password_constraints(nil) === {:error, "The password must be a string."}
  end

  test "cannot set agent password to empty", %{:agent => agent} do
    logger_output =
      capture_log(fn ->
        updated_agent = Agent.set_password(agent, "")
        # agent password should not have been changed.
        assert updated_agent === agent
      end)

    assert logger_output =~
             "New password for agent password_test_agent does not satisfy password constraints. The password cannot be empty. Not setting password."
  end

  test "can set agent password to non-empty string", %{:agent => agent} do
    logger_output =
      capture_log(fn ->
        updated_agent = Agent.set_password(agent, "complex-passw0rd")
        # agent password should have been changed
        refute updated_agent === agent
      end)

    # No password-related error should have been logged.
    refute logger_output =~
             "New password for agent password_test_agent does not satisfy password constraints."
  end
end
