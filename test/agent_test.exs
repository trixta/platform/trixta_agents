defmodule TrixtaAgentsAgentTest do
  use ExUnit.Case
  doctest TrixtaAgents.Agent

  alias TrixtaAgents.Agent

  setup do
    id = UUID.uuid1()

    init_state = %TrixtaAgents.Agent{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Agent",
      current_state: :initialising,
      role_ids: [],
      space_ids: []
    }

    agent = Agent.new(init_state)

    [agent: agent]
  end

  test "new_agent returns the intial state" do
    id = UUID.uuid1()

    init_state = %TrixtaAgents.Agent{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Agent",
      current_state: :initialising
    }

    agent = Agent.new(init_state)

    assert agent.id == id
    assert agent.name == "Genèsis"
    assert agent.description == "The Genèsis Agent"
    assert agent.current_state == :initialising
    assert agent.role_ids == []
    assert agent.space_ids == []
  end

  test "new_agent returns the intial state with default values" do
    id = UUID.uuid1()

    init_state = %TrixtaAgents.Agent{
      id: id
    }

    agent = Agent.new(init_state)

    assert agent.id != nil
    assert agent.name != nil
    assert agent.description != nil
    assert agent.current_state == :initialising
    assert agent.role_ids == []
    assert agent.space_ids == []
  end

  test "new_agent returns error if ID is missing" do
    init_state = %TrixtaAgents.Agent{
      name: "Genèsis",
      description: "The Genèsis Agent",
      current_state: :initialising
    }

    assert_raise ArgumentError, fn -> Agent.new(init_state) end
  end

  test "can get agent summary", context do
    summary = Agent.summarize(context[:agent])

    assert summary.id != nil
    assert summary.name == "Genèsis"
    assert summary.description == "The Genèsis Agent"
    assert summary.current_state == :initialising
    assert summary.role_ids == []
    assert summary.space_ids == []
  end

  test "can update Agent given valid complete new state", %{agent: agent} do
    new_state = %{
      id: agent.id,
      name: "Genèsis Two",
      description: "The Genèsis Two Agent",
      current_state: :active
    }

    {:ok, updated_agent} = Agent.update_state(agent, new_state)

    assert updated_agent.id == agent.id
    assert updated_agent.name == "Genèsis Two"
    assert updated_agent.description == "The Genèsis Two Agent"
    assert updated_agent.current_state == :active
  end

  test "can update Agent given valid partial new state", %{agent: agent} do
    new_state = %{
      name: "Genèsis Three",
      current_state: :waiting
    }

    {:ok, updated_agent} = Agent.update_state(agent, new_state)

    assert updated_agent.id == agent.id
    assert updated_agent.name == "Genèsis Three"
    assert updated_agent.description == "The Genèsis Agent"
    assert updated_agent.current_state == :waiting
  end

  test "can reject invalid agent authentication credentials", context do
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    agent = Agent.set_password(context[:agent], "correct_test_password")
    agent = Agent.get_jwt(agent, "wr0ngpa55")
    assert agent.jwt == {:error, :invalid}
    jwt = Agent.verify_jwt(agent, agent.jwt)
    assert jwt == {:error, :invalid}
  end

  test "can verify agent token", context do
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    agent = Agent.set_password(context[:agent], "correct_test_password")
    agent = Agent.get_jwt(agent, "correct_test_password")
    assert agent.jwt != nil
    jwt = Agent.verify_jwt(agent, agent.jwt)
    assert {:ok, _} = jwt
  end

  test "can reject invalid agent token", context do
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    agent = Agent.set_password(context[:agent], "correct_test_password")
    agent = Agent.get_jwt(agent, "correct_test_password")
    assert agent.jwt != nil

    jwt =
      Agent.verify_jwt(
        agent,
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZ2VudF9pZCI6ImRlYjJmMTcyLThmMjctMTFlOC04ZTNlLTZhMDAwMTNlZmYyMCIsImV4cCI6MzYwMCwidG9rZW5faWQiOiJkZjJjZWMzNDhmMjcxMWU4OGI0ZDZhMDAwMTNlZmYyMCJ9.rKrsL0iXa63gh1QyKWDg-oiIp68UXs7LUSRuZLidiJE"
      )

    assert jwt == {:error, :signature_error}
  end

  test "can reject invalid agent token due to incorrect signature", context do
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    agent = Agent.set_password(context[:agent], "correct_test_password")
    agent = Agent.get_jwt(agent, "correct_test_password")
    assert agent.jwt != nil

    jwt =
      Agent.verify_jwt(
        agent,
        "yJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZ2VudF9pZCI6ImRlYjJmMTcyLThmMjctMTFlOC04ZTNlLTZhMDAwMTNlZmYyMCIsImV4cCI6MzYwMCwidG9rZW5faWQiOiJkZjJjZWMzNDhmMjcxMWU4OGI0ZDZhMDAwMTNlZmYyMCJ9.rKrsL0iXa63gh1QyKWDg-oiIp68UXs7LUSRuZLidiJE"
      )

    assert jwt == {:error, :signature_error}
  end

  test "can reject invalid agent token due to incorrect agent ID", context do
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    agent = Agent.set_password(context[:agent], "correct_test_password")
    agent = Agent.get_jwt(agent, "correct_test_password")
    assert agent.jwt != nil
    agent = Map.put(agent, :id, "fe5e13e0-8f21-11e8-ba02-6a00013eff20")
    jwt = Agent.verify_jwt(agent, agent.jwt)
    assert jwt == {:error, "invalid agent_id"}
  end

  test "can verify agent token without agent id", context do
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    agent = Agent.set_password(context[:agent], "correct_test_password")
    agent = Agent.get_jwt(agent, "correct_test_password")
    assert agent.jwt != nil
    jwt = Agent.verify_jwt(agent.jwt)
    assert {:ok, claims} = jwt
    assert claims["agent_id"] == agent.id
  end

  test "can reject invalid agent token due to incorrect signature without agent id", context do
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    agent = Agent.set_password(context[:agent], "correct_test_password")
    agent = Agent.get_jwt(agent, "correct_test_password")
    assert agent.jwt != nil

    jwt =
      Agent.verify_jwt(
        "yJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZ2VudF9pZCI6ImRlYjJmMTcyLThmMjctMTFlOC04ZTNlLTZhMDAwMTNlZmYyMCIsImV4cCI6MzYwMCwidG9rZW5faWQiOiJkZjJjZWMzNDhmMjcxMWU4OGI0ZDZhMDAwMTNlZmYyMCJ9.rKrsL0iXa63gh1QyKWDg-oiIp68UXs7LUSRuZLidiJE"
      )

    assert jwt == {:error, :signature_error}
  end
end
