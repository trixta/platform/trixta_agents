defmodule TrixtaAgentsAgentsSupervisorTest do
  use ExUnit.Case
  doctest TrixtaAgents.AgentsSupervisor

  alias TrixtaAgents.{AgentServer, AgentsSupervisor}

  setup do
    id = UUID.uuid1()
    id1 = UUID.uuid1()

    agent = %TrixtaAgents.Agent{
      id: id,
      name: "Genesis",
      description: "The Genèsis Agent",
      current_state: :initialising
    }

    child_spec = %{
      id: AgentServer,
      start: {AgentServer, :start_link, [agent]},
      restart: :transient
    }

    {:ok, pid} = start_supervised(AgentServer, child_spec)

    # This hard-coded password is not a security concern
    # because these agents are created specifically for running tests.
    agent = AgentServer.set_password(agent.id, "correct_test_password")

    agent1 = %TrixtaAgents.Agent{
      id: id1,
      name: "Genesis1",
      description: "The Genèsis One Agent",
      current_state: :initialising
    }

    child_spec1 = %{
      id: AgentServer1,
      start: {AgentServer, :start_link, [agent1]},
      restart: :transient
    }

    {:ok, pid1} = start_supervised(AgentServer, child_spec1)

    [agent: agent, agent_id: agent.id, pid: pid, agent1: agent1, agent1_id: agent1.id, pid1: pid1]
  end

  test "agents supervisor restarts agent on server crash", context do
    pid = context[:pid]
    ref = Process.monitor(pid)
    Process.exit(pid, :kill)

    receive do
      {:DOWN, ^ref, :process, ^pid, :killed} ->
        :timer.sleep(1)
        assert is_pid(TrixtaAgents.AgentServer.agent_pid(context[:agent_id]))
    after
      1000 ->
        raise :timeout
    end
  end

  test "can list active agent ids", _context do
    agent_ids_list = AgentsSupervisor.list_agent_ids()
    # assert agent_ids_list == []
    assert length(agent_ids_list) >= 2
  end

  test "can list inactive agent ids", _context do
    active_agent_ids_list = AgentsSupervisor.list_agent_ids()
    assert TrixtaAgents.stop_agent(hd(active_agent_ids_list)) == :ok
    inactive_agent_ids_list = AgentsSupervisor.list_agent_ids(:inactive)
    all_agent_ids_list = AgentsSupervisor.list_agent_ids(:all)
    assert length(inactive_agent_ids_list) < length(all_agent_ids_list)
  end

  test "returns an error for invalid identity credential", %{agent: agent} do
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    AgentServer.set_password(agent.id, "correct_test_password")

    assert {:error, %{reason: "Could not find Agent by id"}} =
             AgentsSupervisor.login_agent("wr0ng1dent1ty", "correct_test_password")
  end

  test "returns an error for invalid password credential", %{agent1: agent} do
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    agent = AgentServer.set_password(agent.id, "correct_test_password")

    assert {:error, %{reason: "Authentication error"}} =
             AgentsSupervisor.login_agent(agent.id, "wr0ngp@55w0rd")
  end

  test "can login agent and get unique JWT", %{agent: agent} do
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    assert {:ok, agent} = AgentsSupervisor.login_agent(agent.id, "correct_test_password")
    assert agent.jwt != nil
    assert agent.agent_id != nil
  end

  test "inactive agent starts up on successful login" do
    # Start an agent in the supervision tree and insert it into dets:
    agent = %TrixtaAgents.Agent{
      id: "InactiveAgent",
      name: "InactiveAgent",
      description: "Inactive Agent",
      current_state: :initialising
    }

    TrixtaAgents.start_agent(agent)
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    AgentServer.set_password(agent.id, "correct_test_password")
    # Stop the agent so that it's inactive but still present in dets:
    assert TrixtaAgents.stop_agent(agent.id)
    refute TrixtaAgents.AgentsSupervisor.is_agent_running(agent.id)

    # Login as this agent and check that its process is started up on login:
    assert {:ok, _} = AgentsSupervisor.login_agent(agent.id, "correct_test_password")
    assert TrixtaAgents.AgentsSupervisor.is_agent_running(agent.id)
    # so that it doesn't interfere with other tests
    TrixtaAgents.terminate_agent(agent.id)
  end

  @tag :pending
  test "can list all agents", _context do
    agents_list = AgentsSupervisor.list_agent_ids(:all)
    assert length(agents_list) == 2
  end

  @tag :pending
  test "can diff agents", _context do
    agents_list = AgentsSupervisor.list_agent_ids(:myers_difference)
    assert length(agents_list) == 2
  end
end
