defmodule TrixtaAgentsAgentServerTest do
  use ExUnit.Case
  doctest TrixtaAgents.AgentServer

  alias TrixtaAgents.AgentServer

  setup do
    id = UUID.uuid1()
    id1 = UUID.uuid1()

    agent = %TrixtaAgents.Agent{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Agent",
      current_state: :initialising
    }

    child_spec = %{
      id: AgentServer,
      start: {AgentServer, :start_link, [agent]},
      restart: :transient
    }

    {:ok, pid} = start_supervised(AgentServer, child_spec)

    agent1 = %TrixtaAgents.Agent{
      id: id1,
      name: "Genèsis One",
      description: "The Genèsis One Agent",
      current_state: :initialising
    }

    child_spec1 = %{
      id: AgentServer1,
      start: {AgentServer, :start_link, [agent1]},
      restart: :transient
    }

    {:ok, pid1} = start_supervised(AgentServer, child_spec1)

    [agent: agent, agent_id: agent.id, pid: pid, agent1: agent1, agent1_id: agent1.id, pid1: pid1]
  end

  test "can get agent summary", context do
    summary = AgentServer.summarize(context[:agent1_id])

    assert summary.id != nil
    assert summary.name == "Genèsis One"
    assert summary.description == "The Genèsis One Agent"
    assert summary.current_state == :initialising
  end

  test "can reject invalid agent authentication credentials", context do
    # These hard-coded passwords are not a security concern
    # because these agents are created specifically for running tests.
    agent = AgentServer.set_password(context[:agent1_id], "correct_test_password", false)
    agent = AgentServer.get_jwt(agent.id, "wr0ngpa55")
    assert agent.jwt == {:error, :invalid}
    jwt = AgentServer.verify_jwt(agent.id, agent.jwt)
    assert jwt == {:error, :invalid}
  end

  test "can verify agent token", context do
    # This hard-coded password is not a security concern
    # because these agents are created specifically for running tests.
    agent = AgentServer.set_password(context[:agent1_id], "correct_test_password", false)
    agent = AgentServer.get_jwt(agent.id, "correct_test_password")
    assert agent.jwt != nil
    jwt = AgentServer.verify_jwt(agent.id, agent.jwt)
    assert {:ok, _} = jwt
  end

  test "can update Agent given valid complete new state", %{agent: agent} do
    new_state = %{
      id: agent.id,
      name: "Genèsis Two",
      description: "The Genèsis Two Agent",
      current_state: :active
    }

    {:ok, updated_agent} = AgentServer.update_state(agent.id, new_state)

    assert updated_agent.id == agent.id
    assert updated_agent.name == "Genèsis Two"
    assert updated_agent.description == "The Genèsis Two Agent"
    assert updated_agent.current_state == :active
  end

  test "can update Agent given valid partial new state", %{agent: agent} do
    new_state = %{
      name: "Genèsis Three",
      current_state: :waiting
    }

    {:ok, updated_agent} = AgentServer.update_state(agent.id, new_state)

    assert updated_agent.id == agent.id
    assert updated_agent.name == "Genèsis Three"
    assert updated_agent.description == "The Genèsis Agent"
    assert updated_agent.current_state == :waiting
  end
end
