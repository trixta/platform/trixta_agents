defmodule TrixtaAgents.AgentStateTest do
  @moduledoc """
    Tests to verify that an agent's state is stored, looked up and searched correctly.
  """
  use ExUnit.Case
  alias TrixtaAgents.AgentServer

  setup do
    id = UUID.uuid1()

    agent = %TrixtaAgents.Agent{
      id: id,
      name: "StateTest",
      description: "The StateTest Agent",
      current_state: :initialising
    }

    child_spec = %{
      id: AgentServer,
      start: {AgentServer, :start_link, [agent]},
      restart: :transient
    }

    {:ok, pid} = start_supervised(AgentServer, child_spec)

    [agent: agent, agent_id: agent.id, pid: pid]
  end

  test "State should be empty on startup", %{agent: agent} do
    assert AgentServer.get_state(agent.id, :internal_state, "prefix:123") === {:ok, nil}
  end

  test "Can store and look up internal state by exact key", %{agent: agent} do
    AgentServer.update_internal_state(agent.id, %{"prefix:123" => %{"foo" => "bar"}})

    assert AgentServer.get_state(agent.id, :internal_state, "prefix:123") ===
             {:ok, %{"foo" => "bar"}}
  end

  test "Can load agent state by prefix", %{agent: agent} do
    AgentServer.update_internal_state(agent.id, %{"prefixXYZ:abc" => %{"foo2" => "bar2"}})
    AgentServer.update_internal_state(agent.id, %{"prefixXYZ:def" => %{"foo3" => "bar3"}})

    # We can look up both values starting with 'prefixXYZ' by using the wildcard character *:
    assert AgentServer.get_all_matching_state(agent.id, :internal_state, "prefixXYZ:*") ===
             {:ok, [%{"foo2" => "bar2"}, %{"foo3" => "bar3"}]}
  end

  test "Can clear internal state", %{agent: agent} do
    AgentServer.update_internal_state(agent.id, %{"foo" => %{"bar" => "baz"}})
    assert AgentServer.get_state(agent.id, :internal_state, "foo") === {:ok, %{"bar" => "baz"}}
    AgentServer.clear_internal_state(agent.id)

    assert AgentServer.get_all_matching_state(agent.id, :internal_state, "prefixXYZ:*") ===
             {:ok, []}

    assert AgentServer.get_state(agent.id, :internal_state, "foo") === {:ok, nil}
  end

  test "Can delete individual KV pairs from internal state", %{agent: agent} do
    AgentServer.update_internal_state(agent.id, %{"key_to_delete" => "will be deleted"})
    AgentServer.update_internal_state(agent.id, %{"key_to_delete2" => "will be deleted2"})

    assert AgentServer.get_state(agent.id, :internal_state, "key_to_delete") ===
             {:ok, "will be deleted"}

    assert AgentServer.get_state(agent.id, :internal_state, "key_to_delete2") ===
             {:ok, "will be deleted2"}

    AgentServer.remove_current_state_kv(agent.id, :internal_state, "key_to_delete")
    assert AgentServer.get_state(agent.id, :internal_state, "key_to_delete") === {:ok, nil}

    assert AgentServer.get_state(agent.id, :internal_state, "key_to_delete2") ===
             {:ok, "will be deleted2"}

    AgentServer.remove_current_state_kv(agent.id, :internal_state, "key_to_delete2")
    assert AgentServer.get_state(agent.id, :internal_state, "key_to_delete") === {:ok, nil}
  end

  test "Can store multiple KV pairs from a single map", %{agent: agent} do
    AgentServer.update_internal_state(agent.id, %{
      "foo" => %{"bar" => "baz"},
      "foo2" => %{"bar2" => "baz2"},
      "foo3" => %{"bar3" => "baz3"}
    })

    assert AgentServer.get_state(agent.id, :internal_state, "foo") === {:ok, %{"bar" => "baz"}}
    assert AgentServer.get_state(agent.id, :internal_state, "foo2") === {:ok, %{"bar2" => "baz2"}}
    assert AgentServer.get_state(agent.id, :internal_state, "foo3") === {:ok, %{"bar3" => "baz3"}}
  end

  test "Can migrate :internal_state map to KV records", %{agent: agent} do
    # Put some records into the :internal_state for us to migrate:

    new_state = %{
      :internal_state => %{"should_migrate_1" => "val1", "should_migrate_2" => "val2"}
    }

    {:ok, _updated_agent} = AgentServer.update_state(agent.id, new_state)

    # We should not be able to access the :internal_state values through the AgentServer yet,
    # because we haven't added them to the storage KV table.
    assert AgentServer.get_state(agent.id, :internal_state, "should_migrate_1") === {:ok, nil}

    # Perform the migration for all agents in storage:
    AgentServer.migrate_internal_state()

    # Check that we can now access the KV record for the two values that we couldn't before:
    assert AgentServer.get_state(agent.id, :internal_state, "should_migrate_1") === {:ok, "val1"}
    assert AgentServer.get_state(agent.id, :internal_state, "should_migrate_2") === {:ok, "val2"}
  end
end
