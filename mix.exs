defmodule TrixtaAgents.MixProject do
  @moduledoc """
  false
  """
  use Mix.Project

  def project do
    [
      app: :trixta_agents,
      version: "0.1.3",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      aliases: aliases(),
      name: "TrixtaAgents",
      docs: [main: "TrixtaAgents", extras: ["README.md"]],
      dialyzer: [plt_add_deps: :transitive],
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
      # if you want to use espec,
      # test_coverage: [tool: ExCoveralls, test_task: "espec"]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {TrixtaAgents.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      {:credo, "~> 1.1", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.1", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.12", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.21", only: [:dev, :test], runtime: false},
      {:inch_ex, "~> 2.0", only: [:dev, :test], runtime: false},
      {:faker_elixir_octopus, "~> 1.0.0", only: [:dev, :test]},
      {:elixir_uuid, "~> 1.2"},
      {:comeonin, "~> 5.1"},
      {:argon2_elixir, "~> 2.1"},
      {:joken, "~> 2.3"},
      {:trixta_names, path: "../trixta_names"},
      {:trixta_roles, path: "../trixta_roles"},
      {:trixta_spaces, path: "../trixta_spaces"},
      {:trixta_storage, path: "../trixta_storage"}
    ]
  end

  def aliases do
    [
      review: ["coveralls", "dialyzer", "inch", "hex.audit", "hex.outdated", "credo --strict"]
    ]
  end

  defp description() do
    "The Trixta Agents Application."
  end

  defp package() do
    [
      licenses: ["AGPL-3.0-or-later"],
      links: %{"GitLab" => "https://gitlab.com/trixta/platform/trixta_agents"}
    ]
  end
end
