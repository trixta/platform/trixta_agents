defmodule TrixtaAgents do
  def start_agent() do
    start_agent(%TrixtaAgents.Agent{id: UUID.uuid1()})
  end

  def start_agent(%TrixtaAgents.Agent{id: nil} = agent) do
    agent = Map.put(agent, :id, UUID.uuid1())
    start_agent(agent)
  end

  def start_agent(%TrixtaAgents.Agent{} = agent) do
    case TrixtaAgents.AgentsSupervisor.start_agent(agent) do
      {:ok, pid} -> {:ok, pid, TrixtaAgents.AgentServer.summarize(pid)}
      {:error, _} = error -> error
      _ -> {:error, {:unknown_error}, agent: agent}
    end
  end

  # If `agent` is a plain map then atomize keys and structify
  def start_agent(%{} = agent) do
    agent =
      agent
      |> Map.new(fn {k, v} ->
        case is_atom(k) do
          false -> {String.to_existing_atom(k), v}
          true -> {k, v}
        end
      end)

    start_agent(struct(TrixtaAgents.Agent, agent))
  end

  def start_agent(agent_id) do
    start_agent(%TrixtaAgents.Agent{id: agent_id})
  end

  def stop_agent(agent_id) do
    TrixtaAgents.AgentsSupervisor.stop_agent(agent_id)
  end

  def terminate_agent(agent_id) do
    TrixtaAgents.AgentsSupervisor.terminate_agent(agent_id)
  end
end
