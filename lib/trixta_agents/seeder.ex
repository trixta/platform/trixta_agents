defmodule TrixtaAgents.Seeder do
  alias FakerElixir, as: Faker

  def cleanup(:i_really_want_to_do_this!) do
    path =
      Application.system_path() <>
        Enum.join(
          [
            Application.environment(:trixta_agents),
            Application.trixta_space_port()
          ],
          "_"
        )

    rm_files_list = Path.wildcard("#{path}*_table")
    Enum.each(rm_files_list, fn f -> File.rm(f) end)
  end

  # This is currently not used because trixta_space will now create the root-agent itself,
  # keeping all the initialization logic in one place.
  # Just leaving it here for future reference and for testing TrixtaAgents in isolation.
  def seed() do
    # Root Role
    {:ok, _root_role_pid, root_role} =
      TrixtaRoles.start_role(%TrixtaRoles.Role{id: "root", name: "root"})

    root_agent = %TrixtaAgents.Agent{id: "root-agent", name: "root-agent"}
    {:ok, _root_agent_pid, root_agent} = TrixtaAgents.start_agent(root_agent)

    # Root Agent Credentials
    TrixtaAgents.AgentServer.set_password(root_agent.id, Application.get_env(:trixta_agents, :root_agent_password))

    # Associate Root Agent with Root Role
    TrixtaAgents.AgentServer.update_state(root_agent.id, %{role_ids: [root_role.id]})
  end

  def seed_more(space) do
    # Child Space 1
    child_space1 = %TrixtaSpaces.Space{
      id: UUID.uuid1(),
      parent_id: space.id,
      name: "Child 1",
      description: "The Child 1 Space",
      current_state: :ready
    }

    TrixtaSpaces.start_space(child_space1)
    # Child Space 2
    child_space2 = %TrixtaSpaces.Space{
      id: UUID.uuid1(),
      parent_id: space.id,
      name: "Child 2",
      description: "The Child 2 Space",
      current_state: :ready
    }

    TrixtaSpaces.start_space(child_space2)
  end

  def seed_loads(_space) do
    # Roles
    roles =
      1..1000
      |> Enum.map(random_role())

    # Spaces
    spaces =
      1..10_000
      |> Enum.map(random_space(roles))

    # Agents
    _agents =
      1..10_000
      |> Enum.map(random_agent(roles, spaces))
  end

  defp random_role() do
    fn _role ->
      role = %TrixtaRoles.Role{
        id: UUID.uuid1(),
        name: Faker.Lorem.words(3),
        description: Faker.Lorem.sentence(),
        current_state: :active
      }

      {:ok, role_pid} = TrixtaRoles.start_role(role)
      TrixtaRoles.RoleServer.summarize(role_pid)
    end
  end

  defp random_space(roles) do
    fn _space ->
      space = %TrixtaSpaces.Space{
        id: UUID.uuid1(),
        parent_id: Enum.take_random(TrixtaSpaces.SpacesSupervisor.list_space_ids(), 1),
        name: Faker.Lorem.words(3),
        description: Faker.Lorem.sentence(),
        current_state: :active,
        role_ids: Enum.map(Enum.take_random(roles, 5), fn role -> role.id end)
      }

      {:ok, space_pid} = TrixtaSpaces.start_space(space)
      TrixtaSpaces.SpaceServer.summarize(space_pid)
    end
  end

  defp random_agent(roles, spaces) do
    fn _agent ->
      agent = %TrixtaAgents.Agent{
        id: UUID.uuid1(),
        name: Faker.Name.name_with_middle(),
        description: Faker.Lorem.sentence(),
        current_state: :active,
        password: "pa55word",
        jwt: nil,
        role_ids: Enum.map(Enum.take_random(roles, 5), fn role -> role.id end),
        space_ids: Enum.map(Enum.take_random(spaces, 50), fn space -> space.id end)
      }

      {:ok, agent_pid} = TrixtaAgents.start_agent(agent)
      TrixtaAgents.AgentServer.summarize(agent_pid)
    end
  end
end
