defmodule TrixtaAgents.AgentServer do
  @moduledoc """
  A agent server process that holds a `Agent` struct as its state.

  # begin Genserver call functions section
  Create functions to make the call to the Genserver in this section, for example
  def summarize(agent_id) do
    GenServer.call(via_tuple(agent_id), :summary) // call to GenServer
  end
  # end Genserver call functions section

  # begin Handle GenServer.call functions section
     Create functions to handle the call to the Genserver in this section, for example

    def handle_call(:summary, _from, agent) do
    {:reply, Agent.summarize(agent), agent} // handle call to GenServer
    end
  # end Handle GenServer.call functions section

  """

  use GenServer

  require Logger

  alias TrixtaAgents.{Agent, AgentsSupervisor}

  @backup_interval :timer.minutes(5)

  # begin Genserver call functions section

  # Client (Public) Interface

  @doc """
  Spawns a new agent server process registered under the given `agent_id`.
  """
  def start_link(agent) do
    GenServer.start_link(
      __MODULE__,
      {agent},
      name: via_tuple(agent.id)
    )
  end

  def summarize(agent_id) do
    GenServer.call(via_tuple(agent_id), :summary)
  end

  def update_state(agent_id, %{} = new_state) do
    GenServer.call(via_tuple(agent_id), {:update_state, new_state})
  end

  def set_password(agent_id, new_password, already_hashed \\ false) do
    GenServer.call(via_tuple(agent_id), {:set_password, new_password, already_hashed})
  end

  def get_jwt(agent_id, password) do
    GenServer.call(via_tuple(agent_id), {:get_jwt, password})
  end

  def verify_jwt(agent_id, jwt) do
    GenServer.call(via_tuple(agent_id), {:verify_jwt, jwt})
  end

  def update_internal_state(agent_id, %{} = new_state_to_merge) do
    GenServer.call(via_tuple(agent_id), {:update_internal_state, new_state_to_merge})
  end

  @doc """
  Get the state of a specific type.
  """
  def get_state(agent_id, type, key) do
    GenServer.call(via_tuple(agent_id), {:get_state, %{:type => type, :key => key}})
  end

  @doc """
  Get the whole agent struct.
  """
  def get_state(agent_id) do
    GenServer.call(via_tuple(agent_id), :get_state)
  end

  def get_all_matching_state(agent_id, type, key) do
    GenServer.call(via_tuple(agent_id), {:get_all_matching_state, %{:type => type, :key => key}})
  end

  def remove_current_state_kv(agent_id, type, key) do
    GenServer.call(via_tuple(agent_id), {:remove_current_state_kv, %{:type => type, :key => key}})
  end

  def clear_internal_state(agent_id) do
    GenServer.call(via_tuple(agent_id), :clear_internal_state)
  end

  @doc """
  Completely delete the KV storage table for the given agent.
  This is called when an agent terminates (so it can't be recovered),
  to prevent a build-up of stale tables.
  """
  def delete_kv_table(agent_id) do
    TrixtaStorage.delete_table(kv_storage_table_name(agent_id))
  end

  def set_reset_token(agent_id) do
    # 'rescue' is for raises
    try do
      GenServer.call(via_tuple(agent_id), :set_reset_token)
    rescue
      ex ->
        {"error", %{"details" => inspect(ex)}}
    catch
      :exit, reason ->
        {"error", %{"details" => inspect(reason)}}

      # for throws
      other ->
        # this is showing too much information
        {"error", %{"details" => inspect(other)}}
        # after
    end
  end

  def set_verify_token(agent_id) do
    # 'rescue' is for raises
    try do
      GenServer.call(via_tuple(agent_id), :set_verify_token)
    rescue
      ex ->
        {"error", %{"details" => inspect(ex)}}
    catch
      :exit, reason ->
        {"error", %{"details" => inspect(reason)}}

      # for throws
      other ->
        # this is showing too much information
        {"error", %{"details" => inspect(other)}}
        # after
    end
  end

  def reset_password(agent_id, new_password, reset_token) do
    GenServer.call(via_tuple(agent_id), {:reset_password, new_password, reset_token})
  end

  # end GenServer.call functions section

  # begin Handle GenServer.call functions section

  @doc """
  Returns a tuple used to register and lookup a agent server process by agent_id.
  """
  def via_tuple(agent_id) when is_pid(agent_id) do
    agent_id
  end

  def via_tuple(agent_id) do
    {:via, :global, {TrixtaAgents.AgentRegistry, agent_id}}
  end

  @doc """
  Returns the `pid` of the agent server process registered under the given `agent_id`, or `nil` if no process is registered.
  """
  def agent_pid(agent_id) do
    agent_id
    |> via_tuple()
    |> GenServer.whereis()
  end

  # Server Callbacks

  def init(agent) do
    {agent} = agent
    agent_id = agent.id

    agent =
      case TrixtaStorage.lookup(:agents_table, agent.id) do
        [] ->
          new_agent = Agent.new(agent)
          TrixtaStorage.save(:agents_table, agent.id, new_agent)
          new_agent

        [{^agent_id, agent}] ->
          Logger.info("Agent with ID '#{agent_id}' already exists. Restarting...")
          agent
      end

    # Key / value table for storing this agent's custom kv pairs.
    init_key_value_storage_table(agent)

    # When this process exits, we don't want the associated KV storage table hanging around.
    # The 'terminate' function will take care of this if we trap exits as follows:
    Process.flag(:trap_exit, true)

    Logger.info("Spawned agent server process for agent ID '#{agent.id}'.")

    schedule_backup()
    {:ok, agent}
  end

  def handle_call(:summary, _from, agent) do
    {:reply, Agent.summarize(agent), agent}
  end

  def handle_call({:update_state, new_state}, _from, agent) do
    case Agent.update_state(agent, new_state) do
      {:ok, new_agent} ->
        TrixtaStorage.save(:agents_table, my_agent_id(), new_agent)
        {:reply, {:ok, Agent.summarize(new_agent)}, new_agent}

      {:error, messages} ->
        {:reply, {:error, messages}}
    end
  end

  @doc """
  Get the whole agent struct.
  """
  def handle_call(:get_state, _from, agent) do
    {:reply, agent, agent}
  end

  def handle_call({:get_state, %{:type => :internal_state, :key => key}}, _from, agent) do
    # Internal state lives in this agent's KV storage table. Fetch it from there:
    {:reply, {:ok, lookup_internal_state(agent, key)}, agent}
  end

  def handle_call({:get_state, %{:type => type, :key => key}}, _from, agent) do
    # For types other than :internal_state, keep using the in-memory Agent state for now:
    {:reply, {:ok, Agent.get_state(agent, type, key)}, agent}
  end

  def handle_call(
        {:get_all_matching_state, %{:type => :internal_state, :key => key}},
        _from,
        agent
      ) do
    # Internal state lives in this agent's KV storage table. Fetch it from there:
    {:reply, {:ok, lookup_internal_state(agent, key, [])}, agent}
  end

  def handle_call({:get_all_matching_state, %{:type => type, :key => key}}, _from, agent) do
    # For types other than :internal_state, keep using the in-memory Agent state for now:
    {:reply, {:ok, Agent.get_all_matching_state(agent, type, key)}, agent}
  end

  @doc """
  Removes a key/value pair from this agent's TrixtaStorage table.
  :internal_state is handled through TrixtaStorage.
  All other types are handled in-memory by the Agent GenServer, as coded in the function below this one.
  """
  def handle_call(
        {:remove_current_state_kv, %{:type => :internal_state, :key => key}},
        _from,
        agent
      ) do
    case TrixtaStorage.delete(kv_storage_table_name(agent), key) do
      :ok -> {:reply, {:ok, %{}}, agent}
      err -> {:reply, {:error, err}, agent}
    end
  end

  def handle_call({:remove_current_state_kv, %{:type => type, :key => key}}, _from, agent) do
    # All state types other than :internal_state gets handled by the Agent GenServer.
    case Agent.remove_current_state_kv(agent, type, key) do
      {:ok, new_agent} ->
        TrixtaStorage.save(:agents_table, my_agent_id(), new_agent)
        {:reply, {:ok, %{}}, new_agent}

      {:error, messages} ->
        {:reply, {:error, messages}}
    end
  end

  def handle_call(:clear_internal_state, _from, agent) do
    case TrixtaStorage.delete_all(kv_storage_table_name(agent)) do
      :ok ->
        {:reply, :ok, agent}

      err ->
        Logger.error(
          "Could not clear internal state of agent #{agent.id} . Error from TrixtaStorage: #{
            inspect(err)
          }"
        )

        {:reply, {:error, err}}
    end
  end

  def handle_call({:set_password, new_password, already_hashed}, _from, agent) do
    new_agent = Agent.set_password(agent, new_password, already_hashed)
    TrixtaStorage.save(:agents_table, my_agent_id(), new_agent)
    {:reply, Agent.summarize(new_agent), new_agent}
  end

  def handle_call(:set_verify_token, _from, agent_state) do
    updated_agent = Agent.set_verify_token(agent_state)

    TrixtaStorage.save(:agents_table, my_agent_id(), updated_agent)

    {:reply, Agent.summarize(updated_agent), updated_agent}
  end

  def handle_call({:get_jwt, password}, _from, agent) do
    new_agent = Agent.get_jwt(agent, password)

    TrixtaStorage.save(:agents_table, my_agent_id(), new_agent)

    {:reply, Agent.summarize(new_agent), new_agent}
  end

  def handle_call({:verify_jwt, jwt}, _from, agent) do
    jwt_struct = Agent.verify_jwt(agent, jwt)

    {:reply, jwt_struct, agent}
  end

  def handle_info(:backup, agent) do
    # TODO: Do backup of the state here
    # Logger.info(
    #   "State for agent server process with agent ID '#{agent.id}' would be backed up now."
    # )

    schedule_backup()
    {:noreply, agent}
  end

  @doc """
  Updates a Key / Value pair (or more than one) in the agent's internal state.
  There is a specific dets table for storing this agent's kv pairs.

  If new_state_to_merge has more than one element, multiple calls are made to TrixtaStorage
  to save a kv entry for each one.
  """
  def handle_call({:update_internal_state, kv_map}, _from, agent_state) when is_map(kv_map) do
    # Start with an :ok result and reduce over the kv pairs,
    # switching to an :error result if one of them fails.
    result =
      kv_map
      |> Enum.reduce(
        {:ok, %{}},
        fn {key, new_value}, result ->
          case TrixtaStorage.save(kv_storage_table_name(agent_state), key, new_value) do
            # No error yet, so don't set the final result to error.
            :ok ->
              result

            err ->
              Logger.error(
                "Could not save kv pair in AgentServer. Key: #{key} . Error: #{inspect(err)}"
              )

              # One of the kv saves have failed, so report that in the final result.
              # We'll still try to save the remaining items though.
              {
                :error,
                ["Could not save kv pair in AgentServer. Key: #{key} . Error: #{inspect(err)}"]
              }
          end
        end
      )

    # We don't update the agent's GenServer state
    # because TrixtaStorage is now the real-time source of truth of the agent's state.
    {:reply, result, agent_state}
  end

  def handle_call({:update_internal_state, _kv_map}, _from, agent_state) do
    Logger.error("Cannot update internal state of trixta_agent. Provided data was not a map.")

    {
      :reply,
      {:error, ["Cannot update internal state of trixta_agent. Provided data was not a map."]},
      agent_state
    }
  end

  def handle_call(:set_reset_token, _from, agent_state) do
    updated_agent = Agent.set_reset_token(agent_state)

    TrixtaStorage.save(:agents_table, my_agent_id(), updated_agent)

    {:reply, Agent.summarize(updated_agent), updated_agent}
  end

  def handle_call({:verify_agent, verify_token}, _from, agent_state) do
    case Agent.verify_agent(agent_state, verify_token) do
      {:ok, updated_agent} ->
        TrixtaStorage.save(:agents_table, my_agent_id(), updated_agent)
        {:reply, {:ok, Agent.summarize(updated_agent)}, updated_agent}

      {:error, error_details} ->
        {
          :reply,
          {:error, error_details},
          agent_state
        }
    end
  end

  def handle_call({:reset_password, new_password, reset_token}, _from, agent_state) do
    updated_agent = Agent.reset_password(agent_state, new_password, reset_token)

    TrixtaStorage.save(:agents_table, my_agent_id(), updated_agent)

    {:reply, Agent.summarize(updated_agent), updated_agent}
  end

  # end Handle GenServer.call functions section

  @doc """
  Called by the VM when the GenServer is about to exit.
  Used to properly close this agent's KV storage table.
  """
  def terminate(_reason, agent) do
    Logger.info("AgentServer terminating: #{agent.id} . Closing KV storage table.")
    TrixtaStorage.close_table(kv_storage_table_name(agent))
  end

  defp my_agent_id do
    ids = :global.registered_names()

    id =
      Enum.find_value(
        ids,
        fn id ->
          if :global.whereis_name(id) == self(), do: id
        end
      )

    {_, id} = id
    # IO.inspect(id)
    id
  end

  defp schedule_backup() do
    Process.send_after(self(), :backup, @backup_interval)
  end

  @doc """
    Creates dets table for this agent's user-defined key/value pairs,
    or opens the existing one.
  """
  defp init_key_value_storage_table(agent) do
    # Each agent has a uniquely named dets table based on its id.
    # This data should by querible by prefix on the key, so we pass the setting to TrixtaStorage.
    case TrixtaStorage.open_table(kv_storage_table_name(agent), %{:prefix_lookup_enabled => true}) do
      {:ok, table_ref} ->
        {:ok, table_ref}

      err ->
        Logger.error(
          "Could not open storage table for agent key value pairs. Agent id: #{agent.id} . err: #{
            inspect(err)
          }"
        )

        :error
    end
  end

  defp kv_storage_table_name(agent_id) when is_binary(agent_id) do
    :"agent_kv_pairs_#{agent_id}"
  end

  defp kv_storage_table_name(agent) do
    kv_storage_table_name(agent.id)
  end

  defp lookup_internal_state(agent, key, default_if_empty \\ nil) do
    case TrixtaStorage.lookup(kv_storage_table_name(agent), key, true) do
      # If only one value is returned, don't wrap it in a list.
      [{^key, value}] ->
        value

      [_head | _rest] = list ->
        # More than one value in the list. Extract all the values into a list:
        list
        |> Enum.map(fn {_key, value} ->
          value
        end)

      _ ->
        default_if_empty
    end
  end

  @doc """
  Temporary function for migrating the :internal_state map
  for all agents into a set of KV records in an agent's KV storage table.

  NOTE: You should make a backup before running this,
  because the :internal_state map will be deleted.
  """
  def migrate_internal_state() do
    AgentsSupervisor.list_agent_ids(:all)
    |> Enum.each(fn agent_id -> migrate_internal_state(agent_id) end)

    {:ok, %{}}
  end

  @doc """
  Temporary function for migrating the :internal_state map
  into a set of KV records in an agent's KV storage table.

  NOTE: You should make a backup before running this,
  because the :internal_state map will be deleted.
  """
  def migrate_internal_state(agent_id) do
    Logger.info("Migrating internal state for agent: #{agent_id}")
    agent = AgentsSupervisor.get_agent_by_id(agent_id)
    AgentsSupervisor.ensure_agent_started(agent)

    # The AgentServer code will create a TrixtaStorage KV record
    # for every item in the map.
    update_internal_state(agent.id, agent.internal_state)

    # Now delete the state from the :internal_state map:
    case update_state(agent.id, %{:internal_state => %{}}) do
      {:ok, updated_agent} ->
        Logger.info("Agent KV migration complete for agent #{agent.id}")
        {:ok, updated_agent}

      err ->
        Logger.error(
          "Error while clearing :internal_state map after agent state migration. Agent id: #{
            agent.id
          } , Error: #{inspect(err)}"
        )

        {:error, err}
    end
  end
end
