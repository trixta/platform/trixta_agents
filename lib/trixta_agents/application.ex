defmodule TrixtaAgents.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: TrixtaAgents.Worker.start_link(arg)
      # {TrixtaAgents.Worker, arg},
      TrixtaAgents.AgentsSupervisor
    ]

    TrixtaStorage.open_table(:agents_table)

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TrixtaAgents.Supervisor]
    ret = Supervisor.start_link(children, opts)

    # load any existing agents
    TrixtaStorage.traverse(:agents_table, fn {_agent_id, agent} ->
      case TrixtaAgents.start_agent(agent) do
        {:error, {:already_started, _pid}} -> :continue
        {:error, {:unknown_error}, _} -> :continue
        {:ok, _pid, _agent} -> :continue
      end
    end)

    ret
  end
end
