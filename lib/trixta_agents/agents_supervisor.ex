defmodule TrixtaAgents.AgentsSupervisor do
  @moduledoc """
  A supervisor that starts `AgentServer` processes dynamically.
  """

  use DynamicSupervisor

  require Logger

  alias TrixtaAgents.{Agent, AgentServer}

  def start_link(_arg) do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  @doc """
  Starts a `AgentServer` process given a Agent struct and supervises it.
  """
  def start_agent(agent) do
    child_spec = %{
      id: AgentServer,
      start: {AgentServer, :start_link, [agent]},
      restart: :transient
    }

    DynamicSupervisor.start_child(__MODULE__, child_spec)
  end

  @doc """
  Stop the `AgentServer` process normally. It CAN be restarted.
  """
  def stop_agent(agent_id) do
    with child_pid when not is_nil(child_pid) <- AgentServer.agent_pid(agent_id) do
      Logger.info("Stopping agent server process with ID '#{agent_id}'.")
      DynamicSupervisor.terminate_child(__MODULE__, child_pid)
    end
  end

  @doc """
  Terminates the `AgentServer` process AND state normally. It CAN'T be restarted.
  """
  def terminate_agent(agent_id) do
    with child_pid when not is_nil(child_pid) <- AgentServer.agent_pid(agent_id) do
      DynamicSupervisor.terminate_child(__MODULE__, child_pid)

      Logger.info(
        "Stopped agent server process with ID '#{agent_id}'. Also removing it from dets (terminate)"
      )
    end

    TrixtaStorage.delete(:agents_table, agent_id)
    # Also completely delete the KV table for this agent, to prevent buildup of stale tables:
    AgentServer.delete_kv_table(agent_id)
  end

  @doc """
  List Agents
  """
  def list_agent_ids() do
    list_agent_ids(:processes)
  end

  def list_agent_ids(:processes) do
    :global.registered_names()
    |> Enum.filter(fn {module, _name} -> module == TrixtaAgents.AgentRegistry end)
    |> Enum.reduce([], fn {_, name}, names -> [name | names] end)
    |> Enum.sort()
  end

  def list_agent_ids(:all) do
    TrixtaStorage.traverse(:agents_table, fn {agent_id, _agent} ->
      {:continue, agent_id}
    end)
    |> Enum.sort()
  end

  def list_agent_ids(:inactive) do
    MapSet.difference(MapSet.new(list_agent_ids(:all)), MapSet.new(list_agent_ids(:processes)))
    |> MapSet.to_list()
  end

  def list_agent_ids(:difference) do
    MapSet.difference(MapSet.new(list_agent_ids(:all)), MapSet.new(list_agent_ids(:processes)))
    |> MapSet.to_list()
  end

  def list_agent_ids(:myers_difference) do
    List.myers_difference(list_agent_ids(:processes), list_agent_ids(:all))
  end

  def list_agent_summaries() do
    TrixtaStorage.traverse(:agents_table, fn {_agent_id, agent} ->
      {:continue, agent}
    end)
  end

  def get_agent_by_name(agent_name) do
    agent =
      TrixtaStorage.traverse(:agents_table, fn {_agent_id, agent} ->
        if agent.name == agent_name, do: {:done, agent}, else: :continue
      end)

    if length(agent) == 1, do: hd(agent), else: nil
  end

  def get_agent_by_id(agent_id) do
    case TrixtaStorage.lookup(:agents_table, agent_id) do
      [{^agent_id, agent}] -> agent
      _ -> nil
    end
  end

  def is_agent_running(agent_id) do
    AgentServer.agent_pid(agent_id)
    |> is_pid()
  end

  def ensure_agent_started(agent) do
    if not is_agent_running(agent.id) do
      TrixtaAgents.AgentsSupervisor.start_agent(agent)
      :started_new_process
    else
      :already_started
    end
  end

  def login_agent(identity, password) do
    case get_agent_by_id(identity) do
      nil ->
        {:error, %{reason: "Could not find Agent by id"}}

      agent ->
        case Agent.get_jwt(agent, password) do
          %{id: _, jwt: {:error, _}} ->
            {:error, %{reason: "Authentication error"}}

          %{id: agent_id, jwt: jwt} ->
            ensure_agent_started(agent)
            {:ok, %{agent_id: agent_id, jwt: jwt}}
        end
    end
  end
end
