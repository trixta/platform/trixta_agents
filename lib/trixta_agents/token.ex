defmodule TrixtaAgents.Token do
  use Joken.Config

  @impl true
  def token_config do
    default_claims(default_exp: 24 * 60 * 60)
  end
end
