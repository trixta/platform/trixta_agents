defmodule TrixtaAgents.Agent do
  require Logger

  defstruct(
    id: nil,
    name: nil,
    description: "Shiny new Agent.",
    current_state: :initialising,
    password: nil,
    jwt: nil,
    role_ids: [],
    space_ids: [],
    # Note: :internal_state is deprecated. Now stored in a KV storage table for each space.
    # TODO: Remove the struct key once migration function has run on all environments.
    internal_state: %{},
    reset_token: nil
  )

  def summarize(agent) do
    %{
      id: agent.id,
      name: agent.name,
      description: agent.description,
      current_state: agent.current_state,
      password: agent.password,
      jwt: agent.jwt,
      role_ids: agent.role_ids,
      space_ids: agent.space_ids,
      # Note: :internal_state is deprecated. Now stored in a KV storage table for each space.
      # TODO: Remove the struct key once migration function has run on all environments.
      internal_state: agent.internal_state,
      reset_token: agent.reset_token
    }
  end

  def new(%TrixtaAgents.Agent{id: nil}) do
    raise ArgumentError, message: "the argument ID is invalid"
  end

  def new(agent = %TrixtaAgents.Agent{}) do
    agent =
      if agent.name do
        agent
      else
        %{agent | name: TrixtaNames.generate()}
      end

    agent
  end

  def update_state(agent, new_state) do
    new_state =
      new_state
      |> Map.new(fn {k, v} ->
        case is_atom(k) do
          false -> {String.to_existing_atom(k), v}
          true -> {k, v}
        end
      end)

    validate(Map.merge(agent, new_state))
  end

  def update_current_state(agent, new_state, type) do
    # TODO: override or accumulate options
    if Map.has_key?(agent, type) do
      Map.put(agent, type, Map.merge(Map.get(agent, type), new_state))
    else
      Map.put(agent, type, Map.merge(%{}, new_state))
    end
    |> validate()
  end

  @doc """
  Get the state for a specific type.
  Types include :internal_state, :inbound_state, :outbound_state, etc.
  """
  def get_state(agent, type, key) do
    key =
      Map.keys(Map.get(agent, type))
      |> Enum.find(fn k -> String.starts_with?(k, key) end)

    Map.get(Map.get(agent, type), key)
  end

  def get_all_matching_state(agent, type, key) do
    keys =
      Map.keys(Map.get(agent, type))
      |> Enum.filter(fn k -> String.starts_with?(k, key) end)

    Enum.map(
      keys,
      fn key ->
        Map.get(Map.get(agent, type), key)
      end
    )
  end

  def remove_current_state_kv(agent, type, key) do
    Map.put(agent, type, Map.delete(Map.get(agent, type), key))
    |> validate()
  end

  def clear_internal_state(agent) do
    # Note: :internal_state is deprecated. Now stored in a KV storage table for each space.
    # TODO: Remove the struct key once migration function has run on all environments.
    Map.put(agent, :internal_state, %{})
    |> validate()
  end

  def set_reset_token(agent) do
    {:ok, new_agent} = validate(Map.put(agent, :reset_token, UUID.uuid4()))
    new_agent
  end

  def reset_password(agent, new_password, reset_token) do
    case check_password_constraints(new_password) do
      :ok ->
        if agent.reset_token == reset_token and is_binary(new_password) and
             String.trim(new_password) != "" do
          updated_agent = set_password(agent, new_password, false)
          Map.put(updated_agent, :reset_token, nil)
        else
          agent
        end

      {:error, details} ->
        Logger.error(
          "New password for agent #{agent.id} does not satisfy password constraints. #{details} Not resetting password."
        )

        agent
    end
  end

  def set_password(agent, hashed_password, true) do
    # The password has already been checked for constraints and hashed.
    %{agent | password: hashed_password}
  end

  def set_password(agent, password, _already_hashed) do
    # The password hasn't been hashed yet.
    # Check it for constraints and then hash + set it.
    case check_password_constraints(password) do
      :ok ->
        %{agent | password: Argon2.hash_pwd_salt(password)}

      {:error, details} ->
        Logger.error(
          "New password for agent #{agent.id} does not satisfy password constraints. #{details} Not setting password."
        )

        agent
    end
  end

  def set_password(agent, password) do
    # We're not told whether the password has been hashed yet,
    # so assume it hasn't to be safe, so that all the password constraints will be checked.
    set_password(agent, password, false)
  end

  def get_jwt(agent, password) do
    case authenticate(agent, password) do
      true -> %{agent | jwt: generate_jwt(agent)}
      false -> %{agent | jwt: {:error, :invalid}}
    end
  end

  defp authenticate(%{:password => hashed_agent_password} = _agent, password)
       when is_binary(hashed_agent_password) and is_binary(password) do
    Argon2.verify_pass(password, hashed_agent_password)
  end

  defp authenticate(_, _) do
    Logger.warn(
      "Non-string password passed in to Agent authentication, or agent password was never set. Authentication failed."
    )

    false
  end

  def generate_jwt(agent) do
    extra_claims = %{
      "agent_id" => agent.id
    }
    TrixtaAgents.Token.generate_and_sign!(extra_claims)
  end

  def verify_jwt(agent, jwt) do
    case jwt do
      {:error, :invalid} ->
        {:error, :invalid}

      _ ->
        with {:ok, claims} <- TrixtaAgents.Token.verify_and_validate(jwt) do
          if claims["agent_id"] == agent.id do
            {:ok, claims}
          else
            {:error, "invalid agent_id"}
          end
        end
    end
  end

  # If we don't have the agent id, we can't add the with_valiation check but we can still verify the token.
  def verify_jwt(jwt) do
    case jwt do
      {:error, msg} ->
        {:error, msg}

      _ ->
        TrixtaAgents.Token.verify_and_validate(jwt)
    end
  end

  @doc """
  Checks some constraints around what an acceptable password should be.
  For now we just check that it's a non-empty string.
  TODO: Decide on some more constraints: Length, at least one symbol, etc...
  """
  def check_password_constraints(password) when is_binary(password) do
    cond do
      String.trim(password) == "" ->
        {:error, "The password cannot be empty."}

      # TODO: Add more clauses here for other password constraints.
      true ->
        # No issues found with this password.
        :ok
    end
  end

  def check_password_constraints(_) do
    {:error, "The password must be a string."}
  end

  # TODO: Can validate any whole or partial new space state
  # **Validation needs to happen here**
  defp validate(new_state) do
    {:ok, new_state}
    # valid? = {:ok, new_state}

    # case valid? do
    #   {:ok, new_state} ->
    #     {:ok, new_state}

    #   {:error, messages} ->
    #     {:error, messages}
    # end
  end
end
