use Mix.Config

# Reduce the number of rounds so it does not slow down our test suite
config :argon2_elixir, t_cost: 1, m_cost: 8
