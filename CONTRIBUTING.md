# Contributing to Trixta

👍🎉 First off, thanks for taking the time to contribute! 🎉👍

Please take a moment to review this document in order to make the contribution process easy and effective for everyone involved.

Following these guidelines helps to communicate that you respect the time of the developers managing and developing this project. In return, they should reciprocate that respect in addressing your issue or assessing patches and features.

## Using the issue tracker

The [issue tracker](https://gitlab.com/groups/trixta/platform/-/issues) is
the preferred channel for [bug reports](#bugs), [features requests](#features)
and [submitting pull requests](#pull-requests).

<a name="bugs"></a>
## Bug reports

A bug is a _demonstrable problem_ that is caused by the code in the repository.
Good bug reports are extremely helpful - thank you!

Guidelines for bug reports:

1. **Use the issue search** &mdash; check if the issue has already been reported.

2. **Check if the issue has been fixed** &mdash; try to reproduce it using the latest `master` or development branch in the repository.

3. **Isolate the problem** &mdash; ideally create a reduced test case and a live example.

A good bug report shouldn't leave others needing to chase you up for more information. Please try to be as detailed as possible in your report. What is your environment? What steps will reproduce the issue? What browser(s) and OS
experience the problem? What would you expect to be the outcome? All these details will help people to fix any potential bugs.

Example:

> Short and descriptive example bug report title
>
> A summary of the issue and the browser/OS environment in which it occurs. If
> suitable, include the steps required to reproduce the bug.
>
> 1. This is the first step
> 2. This is the second step
> 3. Further steps, etc.
>
> `<url>` - a link to the reduced test case
>
> Any other information you want to share that is relevant to the issue being
> reported. This might include the lines of code that you have identified as
> causing the bug, and potential solutions (and your opinions on their
> merits).


<a name="features"></a>
## Feature requests

Feature requests are welcome. But take a moment to find out whether your idea fits with the scope and aims of the project. It's up to *you* to make a strong case to convince the project's developers of the merits of this feature. Please provide as much detail and context as possible.


<a name="pull-requests"></a>
## Pull requests

Good pull requests - patches, improvements, new features - are a fantastic
help. They should remain focused in scope and avoid containing unrelated
commits.

**Please ask first** before embarking on any significant pull request (e.g.
implementing features, refactoring code, porting to a different language),
otherwise you risk spending a lot of time working on something that the
project's developers might not want to merge into the project.

Please adhere to the coding conventions used throughout a project (indentation,
accurate comments, etc.) and any other requirements (such as test coverage).

Since the `master` branch is what we actually use in production, you should create your own topic branch with a descriptive name.

Adhering to the following process is the best way to get your work
included in the project:

1. [Fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) the project, clone your fork, and configure the remotes:

   ```bash
   # Clone your fork of the repo into the current directory
   git clone https://gitlab.com/<your-username>/trixta_agents.git
   # Navigate to the newly cloned directory
   cd trixta_agents
   # Assign the original repo to a remote called "upstream"
   git remote add upstream https://gitlab.com/trixta/platform/trixta_agents.git
   ```

2. If you cloned a while ago, get the latest changes from upstream:

   ```bash
   git checkout master
   git pull upstream master
   ```

3. Create a new topic branch off the `master` branch to contain your feature, change, or fix:

   ```bash
   git checkout -b <topic-branch-name>
   ```

4. Commit your changes in logical chunks. Please adhere to these [git commit message guidelines](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html) or your code is unlikely be merged into the main project. Use Git's [interactive rebase](https://docs.gitlab.com/ee/workflow/gitlab_flow.html#squashing-commits-with-rebase) feature to tidy up your commits before making them public.

5. Locally merge (or rebase) the upstream `master` branch into your topic branch:

   ```bash
   git pull [--rebase] upstream master
   ```

6. Push your topic branch up to your fork:

   ```bash
   git push origin <topic-branch-name>
   ```

7. [Open a Pull Request](https://docs.gitlab.com/ee/workflow/gitlab_flow.html#mergepull-requests-with-gitlab-flow)
    with a clear title and description.

**IMPORTANT**: By submitting a patch, you agree to allow the project
owners to license your work under the terms of the [LICENSE file](https://gitlab.com/trixta/platform/trixta_space/blob/master/LICENSE). Contributions will only be considered if we have an ICLA/CCLA on file for you (see [Contributor Agreements](#agreements)) below.

# Collaboration guidelines
You can find the list of all maintainers in [MAINTAINERS.md](./MAINTAINERS.md).

There are a few basic rules to ensure high quality of the Trixta code:

 - Before merging, an MR requires at least two approvals from the collaborators unless it's an architectural change, a large feature, etc. If it is, then at least 50% of the core team have to agree to merge it, with every team member having a full veto right. (i.e. every single one can block any MR)
 - A MR should remain open for at least two days before merging (does not apply for trivial contributions like fixing a typo). This way everyone has enough time to look into it.

<a name="agreements"></a>
# Contributor Agreements

> Adapted from [https://www.apache.org/licenses/contributor-agreements.html](https://www.apache.org/licenses/contributor-agreements.html) © The Apache Software Foundation

Trixta uses Contributor License Agreements to accept regular contributions from individuals and corporations.

These agreements help us achieve our goal of providing reliable and long-lived software products through collaborative open source software development. In all cases, contributors retain full rights to use their original contributions for any other purpose outside of Trixta while providing Trixta and its projects the right to distribute and build upon their work within Trixta.

## Contributor License Agreements

- [ICLA: Individual Contributor License Agreement](./ICLA.pdf)
- [CCLA: Corporate Contributor License Agreement](./CCLA.pdf)

Trixta desires that all contributors of ideas, code, or documentation to any Trixta projects complete, sign, and submit via email an Individual Contributor License Agreement (ICLA).

The purpose of this agreement is to clearly define the terms under which intellectual property has been contributed to Trixta and thereby allow us to defend the project should there be a legal dispute regarding the software at some future time. A signed ICLA is required to be on file before any individual contribution(s) will be considered for any Trixta project.

For a corporation that has assigned employees to work on a Trixta project, a Corporate CLA (CCLA) is available for contributing intellectual property via the corporation, that may have been assigned as part of an employment agreement.

Note that a Corporate CLA does not remove the need for every developer to sign their own ICLA as an individual, which covers both contributions which are owned and those that are not owned by the corporation signing the CCLA.

The CCLA legally binds the corporation, so it must be signed by a person with authority to enter into legal contracts on behalf of the corporation.

The ICLA is not tied to any employer you may have, so it is recommended to use one's personal email address in the contact details, rather than an @work address.

Your full name will be published unless you provide an alternative public name. For example if your full name is Andrew Bernard Charles Dickens, but you wish to be known as Andrew Dickens, please enter the latter as your public name.

The email address and other contact details are not published.

---
You are always welcome to discuss and propose improvements to this guideline.
